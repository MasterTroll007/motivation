import React, {Component} from 'react';
import PropTypes from 'prop-types';

const TableHeader = () => {
    return (
        <thead>
        <tr>
            <th>Name</th>
            <th>Sure Name</th>
        </tr>
        </thead>
    );
}

const TableBody = props => {
    const rows = props.userData.map((row, index) => {
        return (
            <tr key={index}>
                <td>{row.name}</td>
                <td>{row.surename}</td>
            </tr>
        );
    });
    return <tbody>{rows}</tbody>;
}

class Table extends Component {
 /*
    how to transfer 'this' into inner scope
    constructor() {
        this.loadData = this.loadData.bind(this);
    }
    loadData () {}*/
    render() {
        const {userData} = this.props;

        return (
            <table>
                <TableHeader />
                <TableBody userData={userData} />
            </table>
        );
    }
}
//type control
Table.propTypes = {
    userData: PropTypes.arrayOf(PropTypes.shape({name: PropTypes.string, surename: PropTypes.string}))
}

export default Table;