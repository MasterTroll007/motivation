import React, {Component} from 'react';
import Table from './Table';

class App extends Component {
    render() {
        const users = [
            {
                'name': 'Charlie',
                'surename': 'Janitor'
            },
            {
                'name': 'Mac',
                'surename': 'Bouncer'
            },
            {
                'name': 'Dee',
                'surename': 'Aspring'
            },
            {
                'name': 'Dennis',
                'surename': 'Bartender'
            }
        ];

        return (
            <div className="container">
                <Table userData={users} />
            </div>
        );
    }
}

export default App;

